const User = require("../models/User");
const Product = require("../models/Product");
const bcrypt = require("bcrypt");
const auth = require("../auth");
const { nextTick } = require("process");

// CHECK IF EMAIL EXISTS (FOR REGISTERING)
module.exports.checkEmailExists = (request, response, next) => {
  return User.find({ email: request.body.email }).then((result) => {
    console.log(request.body.email);
    let message = "";

    if (result.length > 0) {
      message = `Email is already taken. Please register another email.`;
      return response.send(false);
    } else {
      next();
    }
  });
};

// REGISTERING A USER
module.exports.registerUser = (request, response) => {
  let newUser = new User({
    email: request.body.email,
    password: bcrypt.hashSync(request.body.password, 10),
  });

  return newUser
    .save()
    .then((user) => {
      console.log(user);
      response.send(true);
    })
    .catch((error) => {
      console.log(error);
      response.send(false);
    });
};

// GET ALL USERS
module.exports.getAllUsers = (request, response) => {
  return User.find({}).then((result) => {
    console.log(result);
    response.send(result);
  });
};

// USER AUTHENTICATION (Login to create token)
module.exports.loginUser = (request, response) => {
  return User.findOne({
    email: request.body.email,
  }).then((result) => {
    console.log(result);
    if (result === null) {
      response.send({ accessToken: "empty" });
    } else {
      const isPasswordCorrect = bcrypt.compareSync(
        request.body.password,
        result.password
      );

      if (isPasswordCorrect) {
        let token = auth.createToken(result);
        console.log(token);
        return response.send({ accessToken: token });
      } else {
        return response.send({ accessToken: "empty" });
      }
    }
  });
};

// Get profile details

module.exports.profileDetails = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  console.log(userData);

  User.findOne({ _id: userData.id })
    .then((result) => {
      result.password = "****";
      console.log(result);
      return response.send(result);
    })
    .catch((err) => {
      return response.send(err);
    });
};

// UPDATE USER ROLE TO ADMIN (ONLY ADMIN CAN DO THIS)
module.exports.updateRole = (request, response) => {
  let token = request.headers.authorization;
  let userData = auth.decode(token);

  let idToBeUpdated = request.params.userId;

  if (userData.isAdmin) {
    return User.findById(idToBeUpdated)
      .then((result) => {
        let updatedId = { isAdmin: !result.isAdmin };

        return User.findByIdAndUpdate(idToBeUpdated, updatedId, { new: true })
          .then((userDocument) => {
            // userDocument.password = "";
            response.send(`User's role has been updated`);
          })
          .catch((error) => {
            response.send(`error`);
          });
      })
      .catch((error) => {
        response.send(`You must be an Admin to change user's role`);
      });
  }
};

// ADD TO CART (USERS ONLY)
module.exports.addToCart = async (request, response) => {
  let userData = auth.decode(request.headers.authorization);

  if (!userData.isAdmin) {
    let userIdToAddToCart = userData.id;
    let productIdToAddToCart = request.params.productId;
    let quantityToBeAdded = request.body.quantity;

    return await User.findById(userIdToAddToCart)
      .then((result) => {
        result.cart.push({
          productId: productIdToAddToCart,
          quantity: quantityToBeAdded,
        });
        // console.log(result);
        result
          .save()
          .then((success) => response.send(true))
          .catch((error) => {
            console.log(error);
            response.send(false);
          });
      })
      .catch((error) => {
        console.log(error);
        return response.send(false);
      });
  } else {
    return response.send(`You are an admin.`);
  }
};

// CREATE ORDER/CHECKOUT

module.exports.checkoutDirectly = async (request, response) => {
  let userData = auth.decode(request.headers.authorization);
  // let productData =

  if (!userData.isAdmin) {
    let userIdToCheckout = userData.id;
    let productIdToCheckout = request.params.productId;
    let quantityToBeCheckedOut = request.body.quantity;

    let productData = await Product.findById(productIdToCheckout)
      .then((result) => result)
      .catch((error) => {
        console.log(error);
      });

    let totalAmount = quantityToBeCheckedOut * productData.price;

    return await User.findById(userIdToCheckout)
      .then((result) => {
        result.orders.push({
          products: {
            productName: productData.name,
            productQuantity: quantityToBeCheckedOut,
          },
          totalAmount: totalAmount,
        });
        result
          .save()
          .then((success) => response.send(true))
          .catch((error) => {
            console.log(error);
            response.send(false);
          });
      })
      .catch((error) => {
        console.log(error);
        response.send(false);
      });
  } else {
    return response.send(`You are an admin.`);
  }
};
