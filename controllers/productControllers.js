const Product = require("../models/Product");
const auth = require("../auth");

// ADD NEW PRODUCT TO DATABASE (ONLY ADMIN CAN ADD PRODUCT TO DATABASE)
module.exports.addProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  let newProduct = new Product({
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
    quantity: request.body.quantity,
  });

  if (userData.isAdmin) {
    return newProduct
      .save()
      .then((product) => {
        response.send(true);
      })
      .catch((error) => {
        response.send(false);
      });
  } else {
    return response.send(false);
  }
};

// RETRIEVE ACTIVE PRODUCTS (ALL USERS AND ADMIN)
module.exports.getActiveProducts = (request, response) => {
  return Product.find({
    isActive: true,
  })
    .then((active) => {
      response.send(active);
    })
    .catch((error) => {
      response.send(error);
    });
};

// RETRIEVE SPECIFIC PRODUCT (ALL USERS AND ADMIN)
module.exports.getSpecificProduct = (request, response) => {
  const productId = request.params.productId;

  return Product.findById(productId)
    .then((product) => {
      response.send(product);
    })
    .catch((error) => {
      response.send(error);
    });
};

// RETRIEVE ALL PRODUCTS (ADMIN ONLY)
module.exports.getAllProducts = (request, response) => {
  const userData = auth.decode(request.headers.authorization);

  if (userData.isAdmin) {
    return Product.find({})
      .then((allProduct) => {
        console.log(allProduct);

        return response.send(allProduct);
      })
      .catch((error) => {
        response.send(error);
      });
  } else {
    return response.send(false);
  }
};

// UPDATE A PRODUCT (ADMIN)
module.exports.updateProduct = (request, response) => {
  const userData = auth.decode(request.headers.authorization);
  const productId = request.params.productId;

  let updatedProduct = {
    name: request.body.name,
    description: request.body.description,
    price: request.body.price,
    quantity: request.body.quantity,
  };

  if (userData.isAdmin) {
    return Product.findByIdAndUpdate(productId, updatedProduct, { new: true })
      .then((updatedProduct) => {
        return response.send(true);
      })
      .catch((error) => {
        console.log(error);
        return response.send(false);
      });
  } else {
    return response.send(false);
  }
};

// ARCHIVE A PRODUCT
module.exports.archiveProduct = (request, response) => {
  let userData = auth.decode(request.headers.authorization);
  let productId = request.params.productId;

  if (userData.isAdmin) {
    return Product.findById(productId)
      .then((archivedProduct) => {
        console.log(archivedProduct);
        let archivedId = { isActive: !archivedProduct.isActive };

        return Product.findByIdAndUpdate(productId, archivedId, { new: true })
          .then((productDocument) => {
            return response.send(true);
          })
          .catch((error) => {
            console.log(error);
            return response.send(false);
          });
      })
      .catch((error) => {
        console.log(error);

        return response.send(false);
      });
  }
};
