// SETTING UP DEPENDENCIES
const express = require("express");
const mongoose = require("mongoose");
const cors = require("cors");

// ACCESS ROUTES
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");
const app = express();

// DATABASE CONNECTION (MONGODB)
mongoose.set({ strictQuery: false }); // NEW UPDATE FROM MONGODB
mongoose.connect(
  "mongodb+srv://admin:admin@zuittbatch243.m7n9nfd.mongodb.net/capstone2?retryWrites=true&w=majority",
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  }
);

mongoose.connection.on(
  "error",
  console.error.bind(console, "connection error")
);

mongoose.connection.once("open", () =>
  console.log(`You are now connected to MongoDB Atlas`)
);

// MIDDLEWARE
app.use(cors());
app.use(express());

// JSON PAYLOAD PARSER (FOR REQUESTS)
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// FOR ALL USER ROUTES
app.use("/users", userRoutes);

// for all product routes defined in the productRoutes route file
app.use("/products", productRoutes);

// FOR FLEXIBILITY
app.listen(process.env.PORT || 4001, () => {
  console.log(`API is now online on port ${process.env.PORT || 4001}`);
});
